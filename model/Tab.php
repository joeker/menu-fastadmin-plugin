<?php

namespace addons\sdcmenu\model;

class Tab extends Model
{

    

    

    // 表名
    protected $name = 'sdcmenu_tab';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'category_text'
    ];

    public function getCategoryTextAttr($value,$data)
    {
        $config = get_addon_config(Model::AddonName);
        return isset($config['tab'][$data['category']]) 
        ? $config['tab'][$data['category']] : '-';
    }

    







}
