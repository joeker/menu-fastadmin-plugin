<?php
/*
 * @Descripttion: 用户收货地址模型
 * @Author: DanceLynx
 * @Date: 2020-11-17 16:43:10
 * @LastEditors: DanceLynx
 * @LastEditTime: 2020-11-17 16:46:26
 */

namespace addons\sdcmenu\model;

class Address extends Model
{

    // 表名
    protected $name = 'sdcmenu_address';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = true;

    // 定义时间戳字段名
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
    ];

}
